package io.not2excel.lib.asm.identifier;

import io.not2excel.lib.asm.AbstractClassBytesProcessor;

/**
 * Created by Richmond Steele on 9/3/14 at 10:28 AM.
 * All Rights Reserved unless alternate license provided.
 */
public abstract class AbstractIdentifier extends AbstractClassBytesProcessor
{
	/**
	 * Override to shortcircuit the processing and prevent modification to the actual class bytes
	 * Returns the inputted class bytes
	 *
	 * @param name name of class to be processed
	 * @param bytes bytes of class
	 * @return bytes param
	 */
	@Override
	public byte[] process(String name, byte[] bytes)
	{
		super.process(name, bytes);
		return bytes;
	}
}
