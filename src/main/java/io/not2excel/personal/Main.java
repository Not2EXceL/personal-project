/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.not2excel.personal;

import io.not2excel.lib.classloading.ClassEnumerator;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class Main
{
    public static void main(String[] args) throws URISyntaxException, IOException
    {
        ClassEnumerator.getClassesFromExternalDirectory(new File(
                Main.class.getProtectionDomain().getCodeSource().getLocation().toURI()));
    }
}
