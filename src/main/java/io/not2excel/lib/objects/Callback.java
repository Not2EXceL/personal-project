package io.not2excel.lib.objects;

import lombok.Getter;

/**
 * Created by Richmond Steele on 9/4/14 at 3:23 PM.
 * All Rights Reserved unless alternate license provided.
 */
public class Callback
{
	@Getter
	private final String  className;
	@Getter
	private final String  methodName;
	@Getter
	private final String  methodDesc;
	@Getter
	private final String  callName;
	@Getter
	private final String  callDesc;
	@Getter
	private final boolean shortCircuit;

	public Callback(String className, String methodName, String methodDesc, String callName, String callDesc,
	                boolean shortCircuit)
	{
		this.className = className;
		this.methodName = methodName;
		this.methodDesc = methodDesc;
		this.callName = callName;
		this.callDesc = callDesc;
		this.shortCircuit = shortCircuit;
	}

	public Callback(String className, String methodName, String methodDesc, String callName, String callDesc)
	{
		this(className, methodName, methodDesc, callName, callDesc, false);
	}
}
