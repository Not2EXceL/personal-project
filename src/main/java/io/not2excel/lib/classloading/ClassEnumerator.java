/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.not2excel.lib.classloading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Recursive idea based off ClassEnumerator by Dave Dopson
 * (used the recursion idea from him to loop through all inner directories as well)
 * I had a very inefficient method of looping through inner directories.
 * When I saw Dave Dopson's ClassEnumerator I realized I could have just used simple recursion to efficiently loop.
 *
 * @author Richmond Steele
 * @since 8/28/13
 * All rights Reserved
 * Please read included LICENSE file
 */
public class ClassEnumerator
{
    private static final Logger logger = LoggerFactory.getLogger("Class Enumerator");

    /**
     * Parses a directory for jar files and class files
     * <p/>
     * Recurses through if necessary
     *
     * @param directory
     *         directory to parse
     *         *NOTE* directory is the parent directory, and the package is assumed from this
     *         eg. c:/path/package_starts_here/package_continues/class_name
     * @return class array
     */
    public static List<Class<?>> getClassesFromExternalDirectory(final File directory)
    {
        final List<Class<?>> classes = new ArrayList<>();
        File[] listFiles = directory.listFiles();
        if (listFiles != null)
        {
            for (final File file : listFiles)
            {
                try
                {
                    final ClassLoader classLoader = new URLClassLoader(new URL[]{
                            file.toURI().toURL()
                    }, ClassEnumerator.class.getClassLoader());
                    if (file.getName().toLowerCase().trim().endsWith(".class"))
                    {
                        String cName = file.getPath()
                                           .replace(directory.getPath(), "")
                                           .replace(".class", "")
                                           .replace(File.separator, ".");
                        if (cName.startsWith("."))
                            cName = cName.substring(1);
                        classes.add(classLoader.loadClass(cName));
                        logger.info("Loaded Class: {}", cName);
                    }
                    if (file.getName().toLowerCase().trim().endsWith(".jar"))
                    {
                        classes.addAll(getClassesFromJar(file, classLoader));
                    }
                    if (file.isDirectory())
                    {
                        classes.addAll(getClassesFromExternalDirectory(file, directory.getPath()));
                    }
                }
                catch (final MalformedURLException | ClassNotFoundException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return classes;
    }

    /**
     * Parses a directory for jar files and class files
     * <p/>
     * Recurses through if necessary
     *
     * @param directory
     *         directory to parse
     * @param pkg
     *         package to be added
     * @return class array
     */
    public static List<Class<?>> getClassesFromExternalDirectory(final File directory, String pkg)
    {
        final List<Class<?>> classes = new ArrayList<>();
        File[] listFiles = directory.listFiles();
        if (listFiles != null)
        {
            for (final File file : listFiles)
            {
                try
                {
                    final ClassLoader classLoader = new URLClassLoader(new URL[]{
                            file.toURI().toURL()
                    }, ClassEnumerator.class.getClassLoader());
                    if (file.getName().toLowerCase().trim().endsWith(".class"))
                    {
                        String cName = file.getPath()
                                           .replace(pkg, "")
                                           .replace(".class", "")
                                           .replace(File.separator, ".");
                        if (cName.startsWith("."))
                            cName = cName.substring(1);
                        classes.add(classLoader.loadClass(cName));
                        logger.info("Loaded Class: {}", cName);
                    }
                    if (file.getName().toLowerCase().trim().endsWith(".jar"))
                    {
                        classes.addAll(getClassesFromJar(file, classLoader));
                    }
                    if (file.isDirectory())
                    {
                        classes.addAll(getClassesFromExternalDirectory(file, pkg));
                    }
                }
                catch (final MalformedURLException | ClassNotFoundException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return classes;
    }

    /**
     * Returns the class array of all classes within a package
     *
     * @param classe
     *         class to get code source location for
     * @return class array
     */
    public static Class<?>[] getClassesFromPackage(final Class<?> classe)
    {
        final List<Class<?>> classes = new ArrayList<>();
        URI uri = null;
        try
        {
            uri = classe.getProtectionDomain().getCodeSource().getLocation().toURI();
        }
        catch (final URISyntaxException e)
        {
            e.printStackTrace();
        }
        if (uri == null)
        {
            throw new RuntimeException("No uri for "
                                       + classe.getProtectionDomain().getCodeSource().getLocation());
        }
        logger.info("URI: {}", uri.toString());
        classes.addAll(processDirectory(new File(uri), ""));
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Returns all class files inside a jar
     *
     * @param file
     *         jar file
     * @param classLoader
     *         classloader created previously using the jar file
     * @return class list
     */
    public static List<Class<?>> getClassesFromJar(final File file, final ClassLoader classLoader)
    {
        final List<Class<?>> classes = new ArrayList<>();
        try
        {
            final JarFile jarFile = new JarFile(file);
            final Enumeration<JarEntry> enumeration = jarFile.entries();
            while (enumeration.hasMoreElements())
            {
                final JarEntry jarEntry = enumeration.nextElement();
                if (jarEntry.isDirectory() || !jarEntry.getName().toLowerCase().trim().endsWith(".class"))
                {
                    continue;
                }
                classes.add(classLoader.loadClass(jarEntry.getName().replace(".class", "")
                                                          .replace(File.separator, ".")));
            }
            jarFile.close();
        }
        catch (final IOException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return classes;
    }

    /**
     * Processes a directory and retrieves all classes from it and its
     * subdirectories
     * <p/>
     * Recurses if necessary
     *
     * @param directory
     *         directory file to traverse
     * @return list of classes
     */
    private static List<Class<?>> processDirectory(final File directory, final String append)
    {
        final List<Class<?>> classes = new ArrayList<>();
        final String[] files = directory.list();
        for (final String fileName : files)
        {
            String className = null;
            if (fileName.endsWith(".class"))
            {
                className = append + '.' + fileName.replace(".class", "");
            }
            if (className != null)
            {
                classes.add(loadClass(className.substring(1)));
            }
            final File subdir = new File(directory, fileName);
            if (subdir.isDirectory())
            {
                classes.addAll(processDirectory(subdir, append + "." + fileName));
            }
        }
        return classes;
    }

    /**
     * Loads a class based upon the name
     *
     * @param className
     *         name of class (.class is pre removed)
     * @return Class if it was loaded properly
     */
    private static Class<?> loadClass(final String className)
    {
        try
        {
            return Class.forName(className);
        }
        catch (final ClassNotFoundException e)
        {
            throw new RuntimeException("Error loading class '" + className + "'");
        }
    }
}

