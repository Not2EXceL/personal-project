package io.not2excel.lib.asm;

/**
 * Created by Richmond Steele on 9/2/14 at 12:27 PM.
 * All Rights Reserved unless alternate license provided.
 */
public interface IClassBytesProcessor
{
	byte[] process(String name, byte[] bytes);
}
