package io.not2excel.lib.asm;

import jdk.internal.org.objectweb.asm.Opcodes;
import lombok.Getter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Richmond Steele on 9/3/14 at 10:33 AM.
 * All Rights Reserved unless alternate license provided.
 */
public abstract class AbstractClassBytesProcessor implements IClassBytesProcessor
{
	public ClassNode classNode;
	@Getter
	private String    name;

	@Override
	public byte[] process(String name, byte[] bytes)
	{
		this.name = name;
		readClass(bytes);
		run();
		return writeClass().toByteArray();
	}

	public abstract void run();

	public ClassWriter writeClass()
	{
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
		classNode.accept(writer);
		return writer;
	}

	public ClassWriter writeClass(DataOutputStream dataOutputStream) throws IOException
	{
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_FRAMES);
		classNode.accept(writer);
		dataOutputStream.write(writer.toByteArray());
		dataOutputStream.flush();
		dataOutputStream.close();
		return writer;
	}

	public void readClass(byte[] bytes)
	{
		ClassNode node = new ClassNode(Opcodes.ASM4);
		ClassReader reader = new ClassReader(bytes);
		reader.accept(node, 0);
		classNode = node;
	}
}
