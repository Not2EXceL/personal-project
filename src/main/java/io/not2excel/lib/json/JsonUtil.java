package io.not2excel.lib.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.lang.reflect.Type;

/**
 * Created by Richmond Steele on 9/2/14 at 12:47 PM.
 * All Rights Reserved unless alternate license provided.
 */
public class JsonUtil<T>
{
	Class<T> classe;
	@Getter
	@Setter
	File location;

	public JsonUtil(Class<T> c)
	{
		classe = c;
	}

	public JsonUtil(Class<T> c, File loc)
	{
		this(c);
		this.location = loc;
	}

	public void serialize(T t, String fileName) throws IOException
	{
		if(location != null)
		{
			serialize(t, location, fileName);
		}
	}

	public void serialize(T t, File location, String fileName) throws IOException
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonWriter jWriter = new JsonWriter(new BufferedWriter(new FileWriter(new File(location, fileName + ".json"))));
		jWriter.setLenient(true);
		jWriter.setIndent("    ");
		Type type = TypeToken.get(classe).getType();
//		System.out.println(type);
		gson.toJson(t, type, jWriter);
		jWriter.flush();
		jWriter.close();
	}

	public T deserialize(String fileName) throws IOException
	{
		if(location != null)
		{
			return deserialize(location, fileName);
		}
		throw new FileNotFoundException("You need to specify a location.");
	}

	public T deserialize(File location, String fileName) throws IOException
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonReader jReader = new JsonReader(new BufferedReader(new FileReader(new File(location, fileName + ".json"))));
		jReader.setLenient(true);
		Type type = TypeToken.get(classe).getType();
//		System.out.println(type);
		T t = gson.fromJson(jReader, type);
		jReader.close();
		return t;
	}
}
