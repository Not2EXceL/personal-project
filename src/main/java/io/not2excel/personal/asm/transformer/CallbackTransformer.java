package io.not2excel.personal.asm.transformer;

import io.not2excel.lib.asm.transformer.AbstractTransformer;
import io.not2excel.lib.objects.Callback;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.InsnList;

/**
 * Created by Richmond Steele on 9/4/14 at 3:12 PM.
 * All Rights Reserved unless alternate license provided.
 */
public class CallbackTransformer extends AbstractTransformer
{
	private final Callback callback;

	public CallbackTransformer(Callback callback)
	{
		this.callback = callback;
	}

	@Override
	public void run()
	{

	}

	private InsnList generateInsnList() throws Exception
	{
		Type methodReturn = Type.getReturnType(this.callback.getMethodDesc());
		Type callbackReturn = Type.getReturnType(this.callback.getCallDesc());

		if (methodReturn != callbackReturn)
			throw new Exception("Callback and target method types don't match.");

		Type[] methodArgs = Type.getArgumentTypes(this.callback.getMethodDesc());
		Type[] callbackArgs = Type.getArgumentTypes(this.callback.getCallDesc());
		if(callbackArgs.length > methodArgs.length)
			throw new Exception("Callback args are greater than target method args.");
		for(int i = 0; i < callbackArgs.length; ++i)
		{
			if(callbackArgs[i] != methodArgs[0])
				throw new Exception(String.format("Callback arg %d is not equivalent to method arg %d", i, i));
		}

		InsnList insn = new InsnList();

		return insn;
	}
}
